from setuptools import setup

setup(
    name="listpy",
    version="0.0.1",
    description="List the python packages on a pypi index",
    license="License :: OSI Approved :: MIT License",
    author="Ian Norton",
    author_email="inorton@gmail.com",
    url="https://gitlab.com/inorton/listpy",
    py_modules=["listpy"],
    install_requires=[
        "pypi-simple>=1.0.0"
    ],
    entry_points={
        "console_scripts": [
            "listpy=listpy:run",
        ]
    }
)
